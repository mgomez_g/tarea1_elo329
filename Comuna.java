import java.awt.geom.Rectangle2D;
import java.time.chrono.MinguoDate;
import java.util.ArrayList;

public class Comuna {
    private Rectangle2D territory; 
    private double distancia;
    private  double probabilidad_0;
    private  double probabilidad_1;
    private  double probabilidad_2;
    private ArrayList<Individuo> poblacion;
    private ArrayList<Vacunatorio> vacunatorios;
    public Comuna(){
        territory = new Rectangle2D.Double(0, 0, 1000, 1000); // 1000x1000 m²;
    }
    public Comuna(double width, double length, double distancia, double probabilidad_0, double probabilidad_1, double probabilidad_2){
        territory = new Rectangle2D.Double(0,0, width, length);
        this.distancia = distancia;
        this.probabilidad_0 = probabilidad_0;
        this.probabilidad_1 = probabilidad_1;
        this.probabilidad_2 = probabilidad_2;

    }
    public void setVacunatorios(ArrayList<Vacunatorio> vacunatorios){
        this.vacunatorios = vacunatorios;
    }
    public double getWidth() {
        return this.territory.getWidth();
    }
    public double getHeight() {
        return this.territory.getHeight();
    }
    public void setPoblacion(ArrayList<Individuo> poblacion){
        this.poblacion = poblacion;
    }
    public void infectionLogic(int i){
        for(int j=(i+1); j<poblacion.size(); j++) {
            if (poblacion.get(i).contactoEstrecho(poblacion.get(j), distancia)) {
                Individuo.Exposicion(poblacion.get(j), poblacion.get(i), probabilidad_0, probabilidad_1, probabilidad_2);
            }
        }
    }//se encarga de revisar si el individuo_i esta cerca de algun individuo_j y de contagiarlo en su proximo estado si es necesario
    public void vaccineLogic(int i){
        if(Vacunatorio.isActive()){
            if(poblacion.get(i).suceptible()) {
                for (int k = 0; k < vacunatorios.size(); k++) {
                    if (vacunatorios.get(k).insideInNextState(poblacion.get(i))) {
                        poblacion.get(i).vacunarse();
                    }
                }
            }
        }
    }//corrobora si el individuo en su proxima posicion se encontrará dentro de un centro de vacunacion, en caso de ser asi, cambia su proximo estado a vacunado
    public void computeNextState (double delta_t) {
        Vacunatorio.computeNextState(delta_t);
        for (int i=0; i<poblacion.size(); i++ ){
            poblacion.get(i).computeNextState(delta_t);
            vaccineLogic(i);
            infectionLogic(i);
            //Se ejecuta primero vaccine logic, pues si infecta antes de entrar al vacunatorio, su proximo estado debe ser infectado, no vacunado.
            //Ejecutando infection logic despues de vaccine logic logramos que el proximo estado sea sobre escrito 
        }
    }//calcula el proximo estado de cada individuo y de los vacunatorios
    public void updateState () {
        for(int i=0; i<poblacion.size(); i++){
            poblacion.get(i).updateState();
        }
    }//actualiza el estado de cada individuo 
    public String getState(){
        int I=0;
        int S=0;
        int R=0;
        int V=0;
        for(int i=0; i<poblacion.size();i++){
            I+= poblacion.get(i).infectado() ? 1:0;
            S+= poblacion.get(i).suceptible() ? 1:0;
            R+= poblacion.get(i).recuperado() ? 1:0;
            V+= poblacion.get(i).vacunado() ? 1:0;
        }
        String output = I+";"+S+";"+R+";"+V;
        return output;
    }//retorna un string con los parametros actuales de la comuna
    public static String getStateDescription(){
        return "I;S;R;V";
    }

 }
